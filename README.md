# assignment2
Software engineering Assignment 2:
===================================
#### Features to implement ####
- [x] Edit book
- [x] Delete book
- [x] New book

## Below are demos of each of the features ##


### Edit Book ###
![](editDemo.gif)

### Delete Book ###

![](deleteDemo.gif)

### New Book ###


![](newBookDemo.gif)

